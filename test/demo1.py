
from locust import HttpUser,TaskSet,task

class UserBehavior(TaskSet):

    def on_start(self):
        pass

    def on_stop(self):
        pass

    @task(1)
    def index(self):
        self.client.get('/',name='首页')

    @task(2) #权重大
    def reserve(self):
        self.client.post("/reserve.php",{"fromPort":"Paris","toPort":"Buenos+Aires"},name='post请求')


class User(HttpUser):
    tasks = [UserBehavior]
    min_wait = 1000
    max_wait = 2000
    stop_timeout = 60
    host = 'http://blazedemo.com'
