## Environment
python => 3

## 安装依赖包

    pip install -r requirements.txt



## 数据库的迁移(数据库迁移不是必要步骤)

初始化：

    (venv)  python manage.py db init 这个命令会在项目下创建 migrations 文件夹，所有迁移脚本都存放其中。


创建第一个版本：

    (venv) python manage.py db migrate


运行升级

    (venv) python manage.py db upgrade

