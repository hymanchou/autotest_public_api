import types
from flask import jsonify,request,current_app
from . import api,login_required
from ..models import PerformResult
from ..util.global_variable import *
from app.models import *
import importlib
from ..util.utils import parse_function,extract_functions
import traceback
import csv

@login_required
@api.route('/perform/find',methods=['POST'])
def get_perform_file():
    """
    读取性能测试脚本
    :return:
    """
    data = request.json
    file_name = data.get('fileName')
    if not file_name:
        return jsonify({'msg':'请输入文件名','status':0})
    if not os.path.exists('{}/{}'.format(PERFORM_FILE_NAME,file_name)):
        return jsonify({'msg':'文件名不存在','status':0})
    with open('{}/{}'.format(PERFORM_FILE_NAME),file_name,'r',encoding='utf-8') as f:
        d = f.read()
    return jsonify({'msg':'获取成功','file_data':d,'status':1})


@api.route('/perform/save',methods=['POST'])
@login_required
def save_perform_file():
    """
    保存性能测试脚本文件
    :return:
    """
    data = request.json
    file_data = data.get('file_data')
    file_name = data.get('fileName')
    if not os.path.exists('{}/{}'.format(PERFORM_FILE_NAME,file_name)):
        return jsonify({'msg':'文件名不存在','status':0})
    with open('{}/{}'.format(PERFORM_FILE_NAME,file_name),'w',encoding='utf8') as f:
        f.write(file_data)
    return jsonify({'msg':'保存成功','status':0})


@api.route('/perform/create',methods=['POST'])
@login_required
def create_file():
    """
    创建性能测试脚本文件
    :return:
    """
    data = request.json
    file_name = data.get('fileName')
    if file_name.find('.py') == -1:
        return jsonify({'msg':'请创建正确格式的py文件','status':0})
    if not file_name:
        return jsonify({'msg':'文件名不能为空','status':0})
    if os.path.exists('{}/{}'.format(PERFORM_FILE_NAME,file_name)):
        return jsonify({'msg':'文件名已经存在','status':0})
    with open('{}/{}'.format(PERFORM_FILE_NAME,file_name),'w',encoding='utf8') as f:
        pass
    return jsonify({'msg':'创建成功','status':1})


@api.route('/perform/execue',methods=['POST'])
@login_required
def perform_exec():
    """
    执行性能测试脚本
    :return:
    """
    data = request.json
    domain = data.get('domain')
    threadCount = data.get('threadCount')
    interval = data.get('interval')
    fileName = data.get('fileName')
    exec_time = data.get('execTime') # 执行时间
    
    if not domain:
        return jsonify({'msg':'请输入域名','status':0})
    if not threadCount or not interval:
        return jsonify({'msg':'并发用户数和间隔时间不能为空','status':0})
    if type(threadCount) != type(int) or type(interval) != type(int):
        return jsonify({'msg':'并发用户数和间隔时间需要为正整数','status':0})
    if not fileName:
        return jsonify({'msg':'文件名不能为空','status':0})
    if not os.path.exists('{}/{}'.format(PERFORM_FILE_NAME,fileName)):
        return jsonify({'msg':'文件不存在','status':0})
    if exec_time < 0:
        return jsonify({'msg':'执行时间不能为负数','status':0})

    perform_file_path = os.getcwd() + os.sep + 'perform_list' + os.sep + fileName
    result_file_path = os.getcwd() + os.sep + 'perform_list' + os.sep + 'result' + os.sep + fileName
    cmd = 'start locust -f ' + perform_file_path + ' --host=' + domain + ' --headless -u ' + threadCount + ' -r ' + interval + ' --run-time ' + exec_time + ' --csv=' + result_file_path
    res = os.system(cmd)  # 执行命令行
    # if res == 0:
    #     # 读取测试结果，并且罗库，然后删除测试文件
    #     return jsonify({'msg':'执行成功','status':1})
    # else:
    #     return jsonify({'msg':'执行失败','status':0})
    return jsonify({'msg':'测试执行中','status':1})



@login_required
@api.route('/perform/check',methods=['POST'])
def perform_check_result():
    """
    查看执行结果
    :return:
    """
    data = request.json
    file_name = data.get('file_name')
    result_file = os.getcwd() + os.sep + 'perform_list' + os.sep + 'result' + os.sep + file_name + '_stats.csv'
    if os.path.exists(result_file):
        # 读取csv文件
        performResult = PerformResult()
        with open(result_file,'r') as f:
            reader = csv.reader(f)
            for i in reader:
                performResult.name = file_name
                performResult.request_count = i[2]
                performResult.failure_count = i[3]
                performResult.median_response_time = i[4]
                performResult.average_response_time = i[5]
                performResult.min_response_time = i[6]
                performResult.max_response_time = i[7]
                performResult.average_content_size = i[8]
                performResult.requests = i[9]
                performResult.failures = i[10]
                db.session.add(performResult)
                db.session.commit()
    else:
        return jsonify({'msg':'文件未生成,稍后重试','status':0})



@login_required
@api.route('/perform/remove',methods=['POST'])
def perform_remove_result():
    """
    移除指定测试结果的文件
    :return:
    """
    data = request.json
    file_name = data.get('file_name')
    for root,dirs,files in os.walk(os.path.abspath('.') + os.sep + 'perform_list'):
        if '.csv' in files:
            files.remove('*.csv')
    return jsonify({'msg':'删除成功','status':1})












