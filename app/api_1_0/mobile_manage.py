from flask import request
from . import api
from app.models import *
from ..util.custom_decorator import *
import json


'''
保存手机信息
'''
@api.route('/phoneinfoupload',methods=['POST'])
def savePhoneInfo():
    data = request.json
    packageName = data.get('packageName')
    systemId = data.get('systemId')
    version = data.get('version')
    phoneModel = data.get('phoneModel')
    deviceName = data.get('deviceName')
    systemTag = data.get('systemTag')
    systemVersion = data.get('systemVersion')

    if packageName is None:
        return jsonify({'msg':'应用包名不能为空','status':0})

    if systemTag is None:
        return jsonify({'msg':'系统tag不能为空','status':0})


    if systemId is None:
        return jsonify({'msg':'系统id不能为空','status':0})
    else:
        phoneInfo = PhoneInfo.query.filter_by(systemId=systemId).first()
        if phoneInfo:
            return jsonify({'msg':'手机信息已经存在','status':0})
        else:
            phoneInfo = PhoneInfo(packageName=packageName,
                                  version=version,
                                  systemId=systemId,
                                  phoneModel=phoneModel,
                                  deviceName=deviceName,
                                  systemTag=systemTag,
                                  systemVersion=systemVersion)
            db.session.add(phoneInfo)
            db.session.commit()
            return jsonify({'msg':'保存成功','status':1})


'''
保存cpu信息
'''
@api.route('/cpuinfoupload',methods=['POST'])
def saveCpuInfo():
    data = request.json
    systemId = data.get('systemid')
    cpuRate = data.get('cpuRate')
    if systemId is None:
        return jsonify({'msg':'系统id不能为空','status':0})
    else:
        phoneInfo = PhoneInfo.query().filter_by(systemId=systemId).first()
        if phoneInfo is None:
            return jsonify({'msg':'手机信息未保存，请先上传手机信息','status':0})
        else:
            phoneInfo_id = phoneInfo.id
            cpuInfo = CpuInfo(cpuRate=cpuRate,phoneinfo_id=phoneInfo_id)
            db.session.add(CpuInfo)
            db.session.commit()
            return jsonify({'msg':'cpu信息保存成功','status':1})

@api.route('/fpsinfoupload',methods=['POST'])
def saveFpsInfo():
    data = request.json
    systemId = data.get('systemid')
    fps = data.get('fps')
    if systemId is None:
        return jsonify({'msg':'系统id不能为空','status':0})
    else:
        phoneInfo = PhoneInfo.query().filter_by(systemId=systemId).first()
    if phoneInfo is None:
        return jsonify({'msg':'手机信息未保存，请先上传手机信息','status':0})
    else:
        phoneInfo_id = phoneInfo.id
        fpsInfo = FpsInfo(fps=fps,phoneinfo_id=phoneInfo_id)
        db.session.add(fpsInfo)
        db.session.commit()
        return jsonify({'msg':'fps信息保存成功','status':1})


@api.route('/meminfoupload',methods=['POST'])
def saveMemInfo():
    data = request.json
    systemId = data.get('systemid')
    used_mem = data.get('used_mem')
    max_mem = data.get('max_mem')
    if systemId is None:
        return jsonify({'msg':'系统id不能为空','status':0})
    else:
        phoneInfo = PhoneInfo.query().filter_by(systemId=systemId).first()
    if phoneInfo is None:
        return jsonify({'msg':'手机信息未保存，请先上传手机信息','status':0})
    else:
        phoneInfo_id = phoneInfo.id
        memInfo = MemInfo(used_mem=used_mem,max_mem=max_mem,phoneinfo_id=phoneInfo_id)
        db.session.add(memInfo)
        db.session.commit()
        return jsonify({'msg':'cpu信息保存成功','status':1})











